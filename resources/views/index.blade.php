<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script
      src="https://kit.fontawesome.com/2ac4b7478a.js"
      crossorigin="anonymous"
    ></script>
    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
    <title>Data Mahasiswa</title>
</head>
<body>
  <section class="vh-100 d-flex justify-content-center align-items-center">
    <div class="col-10 shadow p-5">
      <div class="d-flex justify-content-between align-items-center mb-4">
        <h3>Daftar Mahasiswa</h3>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal" data-bs-whatever="@getbootstrap"> <i class="fa fa-plus"></i> Tambah</button>
      </div>
      <table class="table table-bordered">
          <thead class="table-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama</th>
              <th scope="col">NIM</th>
              <th scope="col">Kelas</th>
              <th scope="col">Prodi</th>
              <th scope="col">Fakultas</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @if (count($mahasiswa) > 0)
              @foreach ($mahasiswa as $i => $mhs)
                <tr>
                  <th>{{$i+1}}</th>
                  <td>{{$mhs->nama_mahasiswa}}</td>
                  <td>{{$mhs->nim_mahasiswa}}</td>
                  <td>{{$mhs->kelas_mahasiswa}}</td>
                  <td>{{$mhs->prodi_mahasiswa}}</td>
                  <td>{{$mhs->fakultas_mahasiswa}}</td>
                  <td>
                    <div class="d-flex justify-content-around">
                      <button type="button" class="btn btn-warning btn-sm mr-5" data-bs-toggle="modal" data-bs-target="#editModal{{$mhs->id}}" data-bs-whatever="@getbootstrap"><i class="fas fa-user-edit text-white"></i></button>
                      <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal{{$mhs->id}}" data-bs-whatever="@getbootstrap"><i class="fas fa-user-minus text-white"></i></button>
                    </div>
                  </td>
                </tr>
                @endforeach
              @endif
          </tbody>
      </table>
      <!-- edit modal -->
      @foreach ($mahasiswa as $mhs)
      <div class="modal fade" id="editModal{{$mhs->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="editModalLabel">Tambah Mahasiswa</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form method="post" action="/edit/{{$mhs->id}}">
              @csrf
                <div class="mb-2">
                  <label for="nama" class="col-form-label">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" value="{{$mhs->nama_mahasiswa}}" required>
                </div>
                <div class="mb-2">
                  <label for="nim" class="col-form-label">NIM</label>
                  <input type="text" class="form-control" id="nim" name="nim" value="{{$mhs->nim_mahasiswa}}" required>
                </div>
                <div class="mb-2">
                  <label for="kelas" class="col-form-label">Kelas</label>
                  <input type="text" class="form-control" id="kelas" name="kelas" value="{{$mhs->kelas_mahasiswa}}" required>
                </div>
                <div class="mb-2">
                  <label for="prodi" class="col-form-label">Prodi</label>
                  <input type="text" class="form-control" id="prodi" name="prodi" value="{{$mhs->prodi_mahasiswa}}" required>
                </div>
                <div class="mb-2">
                  <label for="fakultas" class="col-form-label">Fakultas</label>
                  <input type="text" class="form-control" id="fakultas" name="fakultas" value="{{$mhs->fakultas_mahasiswa}}" required>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
          </div>
        </div>
      </div>
      @endforeach
      <!-- tambah modal -->
      <div class="modal fade" id="addModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addModalLabel">Tambah Mahasiswa</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form method="post" action="/add">
                @csrf
                <div class="mb-2">
                  <label for="nama" class="col-form-label">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama"  required>
                </div>
                <div class="mb-2">
                  <label for="nim" class="col-form-label">NIM</label>
                  <input type="text" class="form-control" id="nim" name="nim"  required>
                </div>
                <div class="mb-2">
                  <label for="kelas" class="col-form-label">Kelas</label>
                  <input type="text" class="form-control" id="kelas" name="kelas" required>
                </div>
                <div class="mb-2">
                  <label for="prodi" class="col-form-label">Prodi</label>
                  <input type="text" class="form-control" id="prodi" name="prodi"  required>
                </div>
                <div class="mb-2">
                  <label for="fakultas" class="col-form-label">Fakultas</label>
                  <input type="text" class="form-control" id="fakultas" name="fakultas"  required>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </form>
          </div>
        </div>
      </div>
      <!-- delete modal modal -->
      @foreach ($mahasiswa as $mhs)
      <div class="modal fade" id="deleteModal{{$mhs->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteModal" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="staticBackdropLabel">Hapus Mahasiswa</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <p>Apakah anda yakin inging menghapus data <strong>{{$mhs->nama_mahasiswa}}</strong>?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
              <a href="/delete/{{$mhs->id}}/{{$mhs->nama_mahasiswa}}" class="btn btn-danger">Hapus</a>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </section>
  @if (session('success'))
  <script>
      alertify.success('<span class="text-white">{{session("success")}}<span>');
  </script>
  @elseif (session('error'))
  <script>
      alertify.error('<span class="text-white">{{session("error")}}<span>');
  </script>
  @endif
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>